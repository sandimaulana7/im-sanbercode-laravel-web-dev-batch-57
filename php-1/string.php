<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php 
    echo "<h3>Contoh Soal 1</h3>";
    $kalimat1 = "PHP is never old";
    echo "Kalimat Pertama :" . $kalimat1 . "<br>"."<br>";
    echo "Panjang Kalimat Pertama : " . strlen($kalimat1) . "<br>";
    echo "Jumlah Kata Kalimat Pertama : " . str_word_count($kalimat1) . "<br>";

    echo "<h3>Contoh Soal 2</h3>";
    $kalimat2 = "Hello Dunia Ku";
    echo "Kalimat Kedua :" . $kalimat2 . "<br>"."<br>";
    echo "Kata ke 1 Kalimat 2 : " . substr($kalimat2,0,5) . "<br>";
    echo "Kata ke 2 Kalimat 2 : " . substr($kalimat2,6,5) . "<br>";
    echo "Kata ke 3 Kalimat 2 : " . substr($kalimat2,12,5) . "<br>";
    
    echo "<h3>Contoh Soal 3</h3>";
    $kalimat3 = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum";
    echo "Kalimat Ketiga :" . $kalimat3 . "<br>"."<br>";
    echo "Kalimat Ketiga Ganti : " . str_replace("Lorem Ipsum", "test 123", $kalimat3);

    ?>


    
</body>
</html>