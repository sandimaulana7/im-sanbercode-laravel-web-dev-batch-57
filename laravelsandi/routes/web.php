<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\UlasanController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'daftar']);
Route::post('/welcome', [AuthController::class, 'masuk']);
Route::get('/master', function (){
    return view('layout.master');
});
Route::get('/table', function (){
    return view('layout.table');
});
Route::get('/data-tables', function (){
    return view('layout.datatable');
});


Route::middleware(['auth'])->group(function () {
    
// CRUD Genre 

// Create Data Genre
Route::get('/genre/create', [GenreController::class,'create']); // tampilan form tambah
Route::post('/genre', [GenreController::class,'store']); //menyimpan data ke database

//Read Data Genre
Route::get('/genre', [GenreController::class,'index']); //menampilkan semua genre dalam tabel genre
Route::get('/genre/{id}', [GenreController::class,'show']); //menampilkan detail
Route::get('/genre/{id}/edit', [GenreController::class,'edit']); //edit data
Route::put('/genre/{id}', [GenreController::class,'update']); //update data

// Delete Data Genre
Route::delete('/genre/{id}', [GenreController::class,'destroy']); //delete data

Route::POST('/ulasan/{film_id}', [UlasanController::class, 'store']);

});

// middleware halaman Genre dan middleware halaman Film kecuali show dan index

Route::get('/genre', [GenreController::class,'index']); //menampilkan semua genre dalam tabel genre
Route::get('/genre/{id}', [GenreController::class,'show']); //menampilkan detail

//CRUD Film
Route::resource('film', FilmController::class);    
Auth::routes();

// CRUD Cast 
// Create Data Cast
Route::get('/cast/create', [CastController::class,'create']); // tampilan form tambah
Route::post('/cast', [CastController::class,'store']); //menyimpan data ke database

//Read Data Cast
Route::get('/cast', [CastController::class,'index']); //menampilkan semua cast dalam tabel cast
Route::get('/cast/{id}', [CastController::class,'show']); //menampilkan detail
Route::get('/cast/{id}/edit', [CastController::class,'edit']); //edit data
Route::put('/cast/{id}', [CastController::class,'update']); //update data

// Delete Data Cast
Route::delete('/cast/{id}', [CastController::class,'destroy']); //delete data






