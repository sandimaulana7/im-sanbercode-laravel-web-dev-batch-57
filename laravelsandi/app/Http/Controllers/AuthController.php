<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('page.register');
    }
    public function masuk(Request $request)
    {
        $namaDepan =  $request->input('fname');
        $namaBelakang = $request->input('lname');
        
        return view('page.dashboard',["namaDepan" => $namaDepan,"namaBelakang" => $namaBelakang]);
    }
}
