<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ulasan;
use Illuminate\Support\Facades\Auth;

class UlasanController extends Controller
{
    public function store ($film_id, Request $request){
        $request->validate([
            'content' => 'required'
        ]);
        Ulasan::create([
            'user_id'=> Auth::id(),
            'film_id'=> $film_id,
            'content'=> $request->input('content')
        ]);
        return redirect('/film/'. $film_id);
    }
}
