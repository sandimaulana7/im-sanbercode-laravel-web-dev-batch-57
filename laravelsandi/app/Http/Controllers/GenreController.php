<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Genre;

class GenreController extends Controller
{
    public function create(){
        return view('genre.tambah');
    }
    public function store(Request $request){

     $request->validate([
            'nama' => 'required|min:3',
        ]);

        DB::table('genre')->insert([
            'nama' => $request->input('nama'),
        ]);
        return redirect ('/genre');
    }
    public function index(){
        $genre=DB::table('genre')->get();
        return view('genre.tampil', ['genre'=> $genre]);
    }
    public function show($id){
        $genre=Genre::find($id);
        return view('genre.detail', ['genre'=>$genre]);
        return $id;
    }
    public function edit($id){
        $genre=DB::table('genre')->find($id);
        return view('genre.edit', ['genre'=>$genre]);
        return $id;
    }
    public function update ($id, Request $request){
        $request->validate([
            'nama' => 'required|min:3',
        ]);

        DB::table('genre')
        ->where('id', $id)
        ->update(
            [
            'nama' => $request->input('nama'),
            ]
            );
            return redirect('/genre');
    }
    public function destroy($id){
        DB::table('genre')->where('id', '=', $id)->delete();
        return redirect('/genre');
    }
}
