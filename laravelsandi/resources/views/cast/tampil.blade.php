@extends('layout.master')
@section('judul')
	Halaman Tambah Cast
@endsection
@section('content')
<a href ="/cast/create" class="btn btn-primary btn-sm">Tambah</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
     @forelse ($cast as $key => $item)
     <tr>
        <th scope="row">{{$key+1}}</th>
        <td>{{$item->nama}}</td>
        <td>{{$item->umur}}</td>
        <td>
          <form action="/cast/{{$item->id}}" method="POST">
            @csrf
            @method("delete")
          <a href="/cast/{{$item->id}}" class="btn btn-sm btn-info">Details</a>
            <a href="/cast/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
            <input type="submit" class="btn btn-sm btn-danger" value="Delete"></input>
          </form>
          </td>
    </tr>
    @empty
    <tr></tr>
  <td>Tidak Ada Cast</td>
    @endforelse

    </tbody>
  </table>
@endsection