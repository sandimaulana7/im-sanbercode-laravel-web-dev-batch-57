@extends('layout.master')
@section('judul')
	Halaman Edit Cast
@endsection
@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
 @method('put')
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    @csrf
    <div class="form-group">
      <label>Nama Lengkap</label>
      <input type="text" value="{{$cast->nama}}"name="nama" class="form-control">
    </div>
    <div class="form-group">
      <label>Umur</label>
      <input type="text" value="{{$cast->umur}}" name="umur" class="form-control">
    </div>
    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" id="" cols="30" rows="10">{{$cast->bio}}</textarea>
        </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection