@extends('layout.master')
@section('judul')
	Halaman Tambah Genre
@endsection
@section('content')
<a href ="/genre/create" class="btn btn-primary btn-sm">Tambah</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
     @forelse ($genre as $key => $item)
     <tr>
        <th scope="row">{{$key+1}}</th>
        <td>{{$item->nama}}</td>
        <td>
          <form action="/genre/{{$item->id}}" method="POST">
            @csrf
            @method("delete")
          <a href="/genre/{{$item->id}}" class="btn btn-sm btn-info">Details</a>
            <a href="/genre/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
            <input type="submit" class="btn btn-sm btn-danger" value="Delete"></input>
          </form>
          </td>
    </tr>
    @empty
    <tr></tr>
  <td>Tidak Ada Genre</td>
    @endforelse

    </tbody>
  </table>
@endsection