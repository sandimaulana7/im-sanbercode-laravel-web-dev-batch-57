<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Form Register</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
@csrf
<label>First Name:</label><br><br>
<input type="text" name="fname"><br><br>
<label>Last Name:</label><br><br>
<input type="text" name="lname"><br><br>
<label>Gender:</label><br><br>
<input type="radio" name="gender" value="1">Male<br>
<input type="radio" name="gender" value="2">Female<br>
<input type="radio" name="gender" value="3">Other<br><br>

<label>Nationaly:</label><br><br>
<select name="national">
<option value="Indonesia">Indonesia</option><br>
<option value="Malaysia">Malaysia</option><br>
<option value="Singapur">Singapur</option><br>
<option value="Other">Other</option><br>
</select><br><br>

<label>Language Spoken:</label><br>
<input type="checkbox" name="language" value="Bahasa Indonesia">Bahasa Indonesia<br>
<input type="checkbox" name="language" value="English">English<br>
<input type="checkbox" name="language" value="Other">Other<br><br>

<label>Bio:</label><br>
<textarea name="bio" cols="40" rows="10"></textarea><br>
<input type="submit" value="Kirim"/><br><br>
<a href="../1. HTML/index.html">Kembali ke Home</a>
    </form>
</body>
</html>