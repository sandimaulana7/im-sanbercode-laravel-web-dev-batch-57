<?php
require_once("animal.php");
require_once("ape.php");
require_once("frog.php");

$sheep= new Animal("shaun");
echo "Nama Hewan : " . $sheep->name . "<br>";
echo "Memiliki Kaki : " . $sheep->legs . "<br>";
echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>";

$kodok = new Frog("buduk");
echo "Nama Hewan : " . $kodok->name . "<br>";
echo "Memiliki Kaki : " . $kodok->legs . "<br>";
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
echo "Jump : " . $kodok->jump();

$sungokong = new Ape("kera sakti");
echo "Nama Hewan : " . $sungokong->name . "<br>";
echo "Memiliki Kaki : " . $sungokong->kaki . "<br>";
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>";
echo "Yell : " . $sungokong->yell();


?>
